import { AllowedComponentProps } from 'vue';
import { App } from 'vue';
import { ComponentCustomProps } from 'vue';
import { ComponentOptions } from 'vue';
import { ComponentPublicInstance } from 'vue';
import { EmitsOptions } from 'vue';
import { PropType } from 'vue';
import { VNode } from 'vue';
import { VNodeProps } from 'vue';
import { WatchOptions } from 'vue';

export declare function Action(params: string): any;

export declare function Action(dest: any, params?: string): any;

export declare function Action(target: any, propertyKey: string): void;

export declare function Action(target: any, propertyKey: string, descriptor: PropertyDescriptor): void;

export declare interface ClassComponentHooks {
    data?(): object;
    beforeCreate?(): void;
    created?(): void;
    beforeMount?(): void;
    mounted?(): void;
    beforeUnmount?(): void;
    unmounted?(): void;
    beforeUpdate?(): void;
    updated?(): void;
    activated?(): void;
    deactivated?(): void;
    render?(): VNode | void;
    errorCaptured?(err: Error, vm: Vue, info: string): boolean | undefined;
    serverPrefetch?(): Promise<unknown>;
}

export declare function createDecorator(factory: (ctx: OptionsContext, key: string, index: number) => void): VueDecorator;

export declare type DataFactory = (ctx: OptionsContext) => () => any;

export declare interface DataPropOptions {
    wrapMode?: DataPropWrapMode;
}

export declare type DataPropWrapMode = 'ref' | 'raw' | 'shallowRef' | 'shallowReactive' | 'shallowReadonly';

declare const _default: {
    install: (app: App, options: VueClassConfig, ...args: any[]) => void;
};
export default _default;

export declare type DefaultFactory<T> = (props: Record<string, unknown>) => T | null | undefined;

export declare type DefaultKeys<P> = {
    [K in keyof P]: P[K] extends WithDefault<any> ? K : never;
}[keyof P];

export declare type ExtractDefaultProps<P> = {
    [K in DefaultKeys<P>]: P[K] extends WithDefault<infer T> ? T : never;
};

export declare type ExtractInstance<T> = T extends VueMixin<infer V> ? V : never;

export declare type ExtractProps<P> = {
    [K in keyof P]: P[K] extends WithDefault<infer T> ? T : P[K];
};

export declare function getOptionsByClass(Ctor: any): any;

export declare function Getter(params: string): any;

export declare function Getter(dest: any, params?: string): any;

export declare function Getter(target: any, propertyKey: string): void;

export declare function Getter(target: any, propertyKey: string, descriptor: PropertyDescriptor): void;

export declare function Inject(params: symbol): any;

export declare function Inject(params: string): any;

export declare function Inject(params: InjectOptions): any;

export declare function Inject(target: any, propertyKey: string): void;

export declare function Inject(target: any, propertyKey: string, descriptor: PropertyDescriptor): void;

export declare interface InjectOptions {
    from?: string | symbol;
    default?: unknown;
}

export declare function mergeSetup(...args: any[]): any;

export declare type MixedVueBase<Mixins extends VueMixin[]> = Mixins extends (infer T)[] ? VueConstructor<UnionToIntersection<ExtractInstance<T>> & Vue> : never;

export declare function mixins<T extends VueMixin[]>(...Ctors: T): MixedVueBase<T>;

export declare function Model(params: PropOptions): any;

export declare function Model(target: any, propertyKey: string): void;

export declare function Model(target: any, propertyKey: string, descriptor: PropertyDescriptor): void;

export declare function Mutation(params: string): any;

export declare function Mutation(target: any, propertyKey: string, descriptor: PropertyDescriptor): void;

export declare function Options<V extends Vue>(constructor: V): <VC extends VueConstructor<VueBase>>(target: VC) => VC;

export declare function Options<V extends Vue>(options: ComponentOptions & ThisType<V>): <VC extends VueConstructor<VueBase>>(target: VC) => VC;

export declare interface OptionsContext {
    options: ComponentOptions;
    dataFactory?: DataFactory;
    beforeHandle?: OptionsFactoryHandle;
    afterHandle?: OptionsFactoryHandle;
    readonly classType: VueConstructor;
    excludeKeys: string[];
    excludeMethods: string[];
    excludeProps: string[];
    dataProps: PropertyDescriptorMap;
    dataPropOptions: Record<string, DataPropOptions>;
    addFactory: (factory: OptionsFactory) => void;
    removeFactory: (name: string) => void;
    hasFactory: (name: string) => boolean;
    clearFactories: () => void;
    getOrderedFactories: () => OptionsFactory[];
    getAttr: (name: string) => any;
    setAttr: (name: string, value: any) => void;
    hasAttr: (name: string) => boolean;
    removeAttr: (name: string) => void;
    clearAttrs: () => void;
}

export declare interface OptionsFactory {
    name: string;
    order: number;
    handle: OptionsFactoryHandle;
}

export declare type OptionsFactoryHandle = (ctx: OptionsContext) => ComponentOptions;

export declare function Prop(params: PropOptions): any;

export declare function Prop(target: any, propertyKey: string): void;

export declare function Prop(target: any, propertyKey: string, descriptor: PropertyDescriptor): void;

export declare interface PropOptions<T = any, D = T> {
    type?: PropType<T> | true | null;
    required?: boolean;
    default?: D | DefaultFactory<D> | null | undefined | object;
    validator?(value: unknown): boolean;
}

export declare function Provide(params: string): any;

export declare function Provide(params: symbol): any;

export declare function Provide(params: ProvideOptions): any;

export declare function Provide(target: any, propertyKey: string): void;

export declare function Provide(target: any, propertyKey: string, descriptor: PropertyDescriptor): void;

export declare interface ProvideOptions {
    name: string | symbol;
    ref?: boolean;
}

export declare type PublicProps = VNodeProps & AllowedComponentProps & ComponentCustomProps;

export declare function setDefaultOptions(options: VueClassConfig): void;

export declare function State(params: string): any;

export declare function State(dest: any, params?: string): any;

export declare function State(target: any, propertyKey: string): void;

export declare function State(target: any, propertyKey: string, descriptor: PropertyDescriptor): void;

export declare type UnionToIntersection<U> = (U extends any ? (k: U) => void : never) extends (k: infer I) => void ? I : never;

export declare type Vue<Props = unknown, Emits extends EmitsOptions = {}, DefaultProps = {}> = ComponentPublicInstance<Props, {}, {}, {}, {}, Emits, PublicProps, DefaultProps, true> & ClassComponentHooks;

export declare const Vue: VueConstructor;

export declare type VueBase = Vue<unknown, never[]>;

export declare interface VueClassConfig {
    hooks?: string[];
    storeType?: "vuex" | "pinia";
    defaultStore?: any;
}

export declare interface VueConstructor<V extends VueBase = Vue> extends VueMixin<V> {
    new (...args: any[]): V;
    storeType: 'vuex' | 'pinia';
    defaultStore: any;
    registerHooks(keys: string[]): void;
    with<P extends {
        new (): unknown;
    }>(Props: P): VueConstructor<V & VueWithProps<InstanceType<P>>>;
}

export declare interface VueDecorator {
    (Ctor: VueConstructor<VueBase>): void;
    (target: VueBase, key: string): void;
    (target: VueBase, key: string, index: number): void;
}

export declare type VueMixin<V extends VueBase = VueBase> = VueStatic & {
    prototype: V;
};

export declare interface VueStatic {
    /* Excluded from this release type: __c */
    /* Excluded from this release type: __b */
    /* Excluded from this release type: __o */
    /* Excluded from this release type: __d */
    /* Excluded from this release type: __h */
    /* Excluded from this release type: __vccOpts */
    /* Excluded from this release type: render */
    /* Excluded from this release type: ssrRender */
    /* Excluded from this release type: __file */
    /* Excluded from this release type: __cssModules */
    /* Excluded from this release type: __scopeId */
    /* Excluded from this release type: __hmrId */
}

export declare type VueWithProps<P> = Vue<ExtractProps<P>, {}, ExtractDefaultProps<P>> & ExtractProps<P>;

export declare function Watch(params: string | string[], watchOptions?: WatchOptions): any;

export declare function Watch(target: any, propertyKey: string, descriptor: PropertyDescriptor): void;

export declare interface WithDefault<T> {
    [withDefaultSymbol]: T;
}

declare const withDefaultSymbol: unique symbol;

export declare function WrapMode(wrapMode: DataPropWrapMode): any;

export { }
