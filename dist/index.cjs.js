/**
  * gowiny-vue-class v1.1.6
  * https://gitee.com/gowiny/vue-class
  *
  * (c) 2022-present gowiny
  * @license MIT
  *
  * Date: 2024-12-05T12:52:44Z
  */
'use strict';

var _slicedToArray = require("@babel/runtime/helpers/slicedToArray");
var _typeof = require("@babel/runtime/helpers/typeof");
var _toConsumableArray = require("@babel/runtime/helpers/toConsumableArray");
var _construct = require("@babel/runtime/helpers/construct");
var _createClass = require("@babel/runtime/helpers/createClass");
var _classCallCheck = require("@babel/runtime/helpers/classCallCheck");
var _possibleConstructorReturn = require("@babel/runtime/helpers/possibleConstructorReturn");
var _getPrototypeOf = require("@babel/runtime/helpers/getPrototypeOf");
var _inherits = require("@babel/runtime/helpers/inherits");
var _defineProperty = require("@babel/runtime/helpers/defineProperty");
function _createForOfIteratorHelper(o, allowArrayLike) { var it = typeof Symbol !== "undefined" && o[Symbol.iterator] || o["@@iterator"]; if (!it) { if (Array.isArray(o) || (it = _unsupportedIterableToArray(o)) || allowArrayLike && o && typeof o.length === "number") { if (it) o = it; var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var normalCompletion = true, didErr = false, err; return { s: function s() { it = it.call(o); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it["return"] != null) it["return"](); } finally { if (didErr) throw err; } } }; }
function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }
function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) arr2[i] = arr[i]; return arr2; }
function ownKeys(e, r) { var t = Object.keys(e); if (Object.getOwnPropertySymbols) { var o = Object.getOwnPropertySymbols(e); r && (o = o.filter(function (r) { return Object.getOwnPropertyDescriptor(e, r).enumerable; })), t.push.apply(t, o); } return t; }
function _objectSpread(e) { for (var r = 1; r < arguments.length; r++) { var t = null != arguments[r] ? arguments[r] : {}; r % 2 ? ownKeys(Object(t), !0).forEach(function (r) { _defineProperty(e, r, t[r]); }) : Object.getOwnPropertyDescriptors ? Object.defineProperties(e, Object.getOwnPropertyDescriptors(t)) : ownKeys(Object(t)).forEach(function (r) { Object.defineProperty(e, r, Object.getOwnPropertyDescriptor(t, r)); }); } return e; }
function _callSuper(t, o, e) { return o = _getPrototypeOf(o), _possibleConstructorReturn(t, _isNativeReflectConstruct() ? Reflect.construct(o, e || [], _getPrototypeOf(t).constructor) : o.apply(t, e)); }
function _isNativeReflectConstruct() { try { var t = !Boolean.prototype.valueOf.call(Reflect.construct(Boolean, [], function () {})); } catch (t) {} return (_isNativeReflectConstruct = function _isNativeReflectConstruct() { return !!t; })(); }
Object.defineProperty(exports, '__esModule', {
  value: true
});
var jsUtils = require('@gowiny/js-utils');
var vue = require('vue');
function getOptionsByClass(Ctor) {
  var options;
  if (Ctor.__vccOpts) {
    options = Ctor.__vccOpts;
  } else {
    options = Ctor;
  }
  return options;
}
function toEmitMap(val) {
  if (!val || !Array.isArray(val)) {
    return val;
  } else {
    var rs = {};
    val.forEach(function (item) {
      rs[item] = null;
    });
    return rs;
  }
}
function mergeSetup() {
  for (var _len = arguments.length, args = new Array(_len), _key = 0; _key < _len; _key++) {
    args[_key] = arguments[_key];
  }
  if (!args) {
    return;
  } else if (args.length == 1) {
    return args[0];
  } else {
    var result = {};
    args.forEach(function (item) {
      if (!item) {
        return;
      } else if (jsUtils.isFunction(item)) {
        result.render = item;
      } else {
        Object.assign(result, item);
      }
    });
    return result;
  }
}
function initDecorator(Ctor) {
  if (!Ctor.__d) {
    Ctor.__d = [];
    Ctor.__d.__ctor = Ctor;
  } else if (Ctor.__d.__ctor !== Ctor) {
    Ctor.__d = [].concat(Ctor.__d);
    Ctor.__d.__ctor = Ctor;
  }
}
function createDecorator(factory) {
  return function (target, key, index) {
    var Ctor = typeof target === 'function' ? target : target.constructor;
    initDecorator(Ctor);
    if (typeof index !== 'number') {
      index = undefined;
    }
    Ctor.__d.push(function (ctx) {
      return factory(ctx, key, index);
    });
  };
}
function mixins() {
  var _MixedVue;
  for (var _len2 = arguments.length, Ctors = new Array(_len2), _key2 = 0; _key2 < _len2; _key2++) {
    Ctors[_key2] = arguments[_key2];
  }
  return _MixedVue = /*#__PURE__*/function (_Vue) {
    _inherits(MixedVue, _Vue);
    function MixedVue() {
      var _this;
      for (var _len3 = arguments.length, args = new Array(_len3), _key3 = 0; _key3 < _len3; _key3++) {
        args[_key3] = arguments[_key3];
      }
      _classCallCheck(this, MixedVue);
      _this = _callSuper(this, MixedVue, [].concat(args));
      Ctors.forEach(function (Ctor) {
        var data = _construct(Ctor, args);
        Object.keys(data).forEach(function (key) {
          _this[key] = data[key];
        });
      });
      return _this;
    }
    return _createClass(MixedVue);
  }(Vue), _defineProperty(_MixedVue, "__b", {
    mixins: Ctors.map(function (Ctor) {
      return Ctor.__vccOpts;
    })
  }), _MixedVue;
}
var EXCLUDE_SUPER_PROPS = ['data', 'setup'];
function defineRawProxy(proxy, key, target) {
  Object.defineProperty(proxy, key, {
    get: function get() {
      return target[key];
    },
    set: function set(value) {
      target[key] = value;
    },
    enumerable: true,
    configurable: true
  });
}
function defineRefProxy(proxy, key, target) {
  Object.defineProperty(proxy, key, {
    get: function get() {
      return target[key].value;
    },
    set: function set(value) {
      target[key].value = value;
    },
    enumerable: true,
    configurable: true
  });
}
function getSuper(Ctor) {
  var superProto = Object.getPrototypeOf(Ctor.prototype);
  if (!superProto) {
    return undefined;
  }
  return superProto.constructor;
}
function getOwn(value, key) {
  return value.hasOwnProperty(key) ? value[key] : undefined;
}
function copyNotExistProps(dest, src, propNames) {
  dest = dest || {};
  if (src) {
    var destPropNames = Object.getOwnPropertyNames(dest);
    propNames = propNames || Object.getOwnPropertyNames(src);
    if (propNames.length > 0) {
      propNames.forEach(function (item) {
        if (destPropNames.indexOf(item) == -1) {
          dest[item] = src[item];
        }
      });
    }
  }
  return dest;
}
function getNotExistProps(src, dest, propNames) {
  var result;
  if (src) {
    propNames = propNames || Object.getOwnPropertyNames(src);
    if (propNames.length > 0) {
      var isNullDest = jsUtils.isNull(dest);
      result = {};
      propNames.forEach(function (item) {
        var srcVal = src[item];
        if (!jsUtils.isNull(srcVal) && (isNullDest || jsUtils.isNull(dest[item]))) {
          result[item] = srcVal;
        }
      });
    }
    result = result || _objectSpread({}, src);
  }
  return result;
}
var defaultBeforeFactoryHandle = function defaultBeforeFactoryHandle(ctx) {
  var options = ctx.options;
  var Ctor = ctx.classType;
  var proto = Ctor.prototype;
  var propNames = Object.getOwnPropertyNames(proto);
  // Inject base options as a mixin
  var base = getOwn(Ctor, '__b');
  if (base) {
    options.mixins = options.mixins || [];
    options.mixins.unshift(base);
  }
  options.methods = _objectSpread({}, options.methods);
  options.computed = _objectSpread({}, options.computed);
  propNames.forEach(function (key) {
    if (key === 'constructor') {
      return;
    }
    // hooks
    if (Ctor.__h.indexOf(key) > -1) {
      options[key] = proto[key];
      return;
    }
    var descriptor = Object.getOwnPropertyDescriptor(proto, key);
    if (key === 'setup') {
      options.setup = descriptor.value;
      return;
    }
    // methods
    if (typeof descriptor.value === 'function') {
      options.methods[key] = descriptor.value;
      return;
    }
    // computed properties
    if (descriptor.get || descriptor.set) {
      options.computed[key] = {
        get: descriptor.get,
        set: descriptor.set
      };
      return;
    }
  });
  // from Vue Loader
  var injections = ['render', 'ssrRender', '__file', '__cssModules', '__scopeId', '__hmrId'];
  injections.forEach(function (key) {
    if (Ctor[key]) {
      options[key] = Ctor[key];
    }
  });
  // Handle super class options
  var Super = getSuper(Ctor);
  if (Super) {
    var superOptions = Super.__vccOpts;
    var extendOptions = getNotExistProps(superOptions, options, Ctor.__h);
    if (!options.components) {
      options.components = {};
    }
    options.components = copyNotExistProps(options.components, superOptions.components);
    options.computed = copyNotExistProps(options.computed, superOptions.computed);
    options.methods = copyNotExistProps(options.methods, superOptions.methods);
    options.watch = copyNotExistProps(options.watch, superOptions.watch);
    options.emits = copyNotExistProps(toEmitMap(options.emits), toEmitMap(superOptions.emits));
    EXCLUDE_SUPER_PROPS.forEach(function (item) {
      delete extendOptions[item];
    });
    Object.assign(options, extendOptions);
    //options.extends = extendOptions
  }
  return options;
};
var defaultDataFactory = function defaultDataFactory(ctx) {
  var result = function result() {
    var Ctor = ctx.classType;
    var excludeProps = ctx.excludeProps;
    var data = new Ctor();
    var plainData = {};
    var dataKeys = Object.keys(data);
    dataKeys.forEach(function (key) {
      var val;
      if (excludeProps.indexOf(key) > -1 || (val = data[key]) && val.__s) {
        return;
      }
      var dataOption = ctx.dataPropOptions[key];
      var wrapMode = dataOption && dataOption.wrapMode ? dataOption.wrapMode : 'ref';
      switch (wrapMode) {
        case 'ref':
          plainData[key] = vue.ref(val);
          defineRefProxy(data, key, plainData);
          break;
        case 'raw':
          plainData[key] = val;
          defineRawProxy(data, key, plainData);
          break;
        case 'shallowRef':
          plainData[key] = vue.shallowRef(val);
          defineRefProxy(data, key, plainData);
          break;
        case 'shallowReactive':
          plainData[key] = vue.shallowReactive(val);
          defineRawProxy(data, key, plainData);
          break;
        case 'shallowReadonly':
          plainData[key] = vue.shallowReadonly(val);
          defineRawProxy(data, key, plainData);
          break;
        default:
          plainData[key] = vue.ref(val);
          defineRefProxy(data, key, plainData);
          break;
      }
    });
    Object.defineProperties(plainData, ctx.dataProps);
    return plainData;
  };
  return result;
};
var defaultAfterFactoryHandle = function defaultAfterFactoryHandle(ctx) {
  var options = ctx.options;
  var excludeKeys = _toConsumableArray(ctx.excludeKeys);
  if (options.computed) {
    excludeKeys.push.apply(excludeKeys, _toConsumableArray(Object.keys(options.computed)));
  }
  if (options.props) {
    excludeKeys.push.apply(excludeKeys, _toConsumableArray(Object.keys(options.props)));
  }
  var excludeMethods = [].concat(_toConsumableArray(excludeKeys), _toConsumableArray(ctx.excludeMethods));
  excludeMethods.forEach(function (key) {
    delete options.methods[key];
  });
  if (options.methods) {
    excludeKeys.push.apply(excludeKeys, _toConsumableArray(Object.keys(options.methods)));
  }
  var excludeProps = [].concat(_toConsumableArray(excludeKeys), _toConsumableArray(ctx.excludeProps));
  ctx.excludeProps = excludeProps;
  return options;
};
var OptionsContextImpl = /*#__PURE__*/function () {
  function OptionsContextImpl(options, target) {
    _classCallCheck(this, OptionsContextImpl);
    _defineProperty(this, "options", void 0);
    _defineProperty(this, "classType", void 0);
    _defineProperty(this, "dataFactory", void 0);
    _defineProperty(this, "beforeHandle", void 0);
    _defineProperty(this, "afterHandle", void 0);
    _defineProperty(this, "attrs", new Map());
    _defineProperty(this, "factoryMap", {});
    _defineProperty(this, "excludeKeys", []);
    _defineProperty(this, "excludeMethods", []);
    _defineProperty(this, "excludeProps", []);
    _defineProperty(this, "dataProps", {});
    _defineProperty(this, "dataPropOptions", {});
    this.options = options;
    this.classType = target;
    this.dataFactory = defaultDataFactory;
    this.beforeHandle = defaultBeforeFactoryHandle;
    this.afterHandle = defaultAfterFactoryHandle;
  }
  _createClass(OptionsContextImpl, [{
    key: "addFactory",
    value: function addFactory(factory) {
      this.factoryMap[factory.name] = factory;
    }
  }, {
    key: "removeFactory",
    value: function removeFactory(name) {
      delete this.factoryMap[name];
    }
  }, {
    key: "hasFactory",
    value: function hasFactory(name) {
      return Reflect.has(this.factoryMap, name);
    }
  }, {
    key: "getOrderedFactories",
    value: function getOrderedFactories() {
      var list = Object.values(this.factoryMap);
      list = list.sort(function (o1, o2) {
        return o1.order - o2.order;
      });
      return list;
    }
  }, {
    key: "clearFactories",
    value: function clearFactories() {
      var _this2 = this;
      var keys = Object.keys(this.factoryMap);
      keys.forEach(function (key) {
        delete _this2.factoryMap[key];
      });
    }
  }, {
    key: "getAttr",
    value: function getAttr(name) {
      return this.attrs.get(name);
    }
  }, {
    key: "hasAttr",
    value: function hasAttr(name) {
      return this.attrs.has(name);
    }
  }, {
    key: "setAttr",
    value: function setAttr(name, value) {
      this.attrs.set(name, value);
    }
  }, {
    key: "removeAttr",
    value: function removeAttr(name) {
      this.attrs["delete"](name);
    }
  }, {
    key: "clearAttrs",
    value: function clearAttrs() {
      this.attrs.clear();
    }
  }]);
  return OptionsContextImpl;
}();
var VueImpl = /*#__PURE__*/function () {
  function VueImpl() {
    _classCallCheck(this, VueImpl);
  }
  _createClass(VueImpl, null, [{
    key: "__vccOpts",
    get: function get() {
      // Early return if `this` is base class as it does not have any options
      if (this === Vue) {
        return {};
      }
      var Ctor = this;
      var cache = getOwn(Ctor, '__c');
      if (cache) {
        return cache;
      }
      initDecorator(Ctor);
      // If the options are provided via decorator use it as a base
      var options = _objectSpread({}, getOwn(Ctor, '__o'));
      var optionsContext = new OptionsContextImpl(options, Ctor);
      var decorators = getOwn(Ctor, '__d');
      if (decorators) {
        decorators.forEach(function (fn) {
          return fn(optionsContext);
        });
      }
      if (optionsContext.beforeHandle) {
        options = optionsContext.beforeHandle(optionsContext);
        optionsContext.options = options;
      }
      var factories = optionsContext.getOrderedFactories();
      for (var i = 0; i < factories.length; i++) {
        var item = factories[i];
        options = item.handle(optionsContext);
        optionsContext.options = options;
      }
      if (optionsContext.afterHandle) {
        options = optionsContext.afterHandle(optionsContext);
        optionsContext.options = options;
      }
      var dataFactory = optionsContext.dataFactory || defaultDataFactory;
      options.data = dataFactory(optionsContext);
      if (!options.name) {
        options.name = Ctor.name;
      }
      Ctor.__c = options;
      return options;
    }
  }, {
    key: "registerHooks",
    value: function registerHooks(keys) {
      var _this3 = this;
      keys.forEach(function (item) {
        if (_this3.__h.indexOf(item) == -1) {
          _this3.__h.push(item);
        }
      });
    }
  }, {
    key: "with",
    value: function _with(Props) {
      var propsMeta = new Props();
      var props = {};
      Object.keys(propsMeta).forEach(function (key) {
        var meta = propsMeta[key];
        props[key] = meta !== null && meta !== void 0 ? meta : null;
      });
      var PropsMixin = /*#__PURE__*/function (_this4) {
        _inherits(PropsMixin, _this4);
        function PropsMixin() {
          _classCallCheck(this, PropsMixin);
          return _callSuper(this, PropsMixin, arguments);
        }
        return _createClass(PropsMixin);
      }(this);
      _defineProperty(PropsMixin, "__b", {
        props: props
      });
      return PropsMixin;
    }
  }]);
  return VueImpl;
}();
_defineProperty(VueImpl, "__h", ['beforeCreate', 'created', 'beforeMount', 'mounted', 'beforeUnmount', 'unmounted', 'beforeUpdate', 'updated', 'activated', 'deactivated', 'render', 'errorCaptured', 'serverPrefetch']);
_defineProperty(VueImpl, "storeType", 'vuex');
_defineProperty(VueImpl, "defaultStore", void 0);
var Vue = VueImpl;
function setDefaultOptions(options) {
  var hooks = options.hooks;
  Vue.storeType = options.storeType || 'vuex';
  Vue.defaultStore = options.defaultStore;
  if (hooks) {
    Vue.registerHooks(hooks);
  }
}
var vuePlugin = {
  install: function install(app, options) {
    app.config.unwrapInjectedRef = true;
    setDefaultOptions(options);
  }
};
function Options(conf) {
  if (typeof conf === 'function') {
    conf.__o = {};
    return conf;
  } else {
    return function (Component) {
      Component.__o = conf;
      return Component;
    };
  }
}
function create$8(params, wopt, target, propertyKey, descriptor) {
  createDecorator(function (ctx, key) {
    var options = ctx.options;
    if (!options.watch) {
      options.watch = {};
    }
    var name = params || key;
    var watch = options.watch;
    var watchOption;
    if (wopt) {
      watchOption = Object.assign({}, wopt, {
        handler: descriptor === null || descriptor === void 0 ? void 0 : descriptor.value
      });
    } else {
      watchOption = {
        handler: descriptor === null || descriptor === void 0 ? void 0 : descriptor.value
      };
    }
    if (Array.isArray(name)) {
      name.forEach(function (item) {
        watch[item] = watchOption;
      });
    } else {
      watch[name] = watchOption;
    }
    ctx.excludeKeys.push(key);
  })(target, propertyKey);
}
function Watch(target) {
  var propertyKey = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
  var descriptor = arguments.length > 2 ? arguments[2] : undefined;
  if (propertyKey && typeof propertyKey === 'string') {
    create$8(propertyKey, undefined, target, propertyKey, descriptor);
  } else {
    var params = target;
    var watchOptions = propertyKey;
    return function (target, propertyKey, descriptor) {
      create$8(params, watchOptions, target, propertyKey, descriptor);
    };
  }
}
function WrapMode(wrapMode) {
  return function (target, propertyKey) {
    createDecorator(function (ctx, key) {
      var conf = ctx.dataPropOptions[key];
      if (!conf) {
        conf = {
          wrapMode: wrapMode
        };
        ctx.dataPropOptions[key] = conf;
      } else {
        conf.wrapMode = wrapMode;
      }
    })(target, propertyKey);
  };
}
function create$7(params, target, propertyKey, descriptor) {
  createDecorator(function (ctx, key) {
    var options = ctx.options;
    if (!options.props) {
      options.props = {};
    }
    var props = options.props;
    var oldDefault = params["default"];
    var defType = _typeof(oldDefault);
    if (descriptor) {
      //定义在方法上
      if (defType === 'undefined') {
        params["default"] = function () {
          return descriptor.value;
        };
      }
    } else {
      //定义在属性上
      if (defType === 'object') {
        params["default"] = jsUtils.clone(oldDefault);
      } else if (defType === 'undefined') {
        params["default"] = jsUtils.getClassDefaultValue(ctx.classType, key);
      }
    }
    props[key] = params;
  })(target, propertyKey);
}
function Prop(target) {
  var propertyKey = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
  var descriptor = arguments.length > 2 ? arguments[2] : undefined;
  if (propertyKey) {
    create$7({}, target, propertyKey, descriptor);
  } else {
    var params = target;
    return function (target, propertyKey, descriptor) {
      create$7(params, target, propertyKey, descriptor);
    };
  }
}
function create$6(params, target, propertyKey, descriptor) {
  create$7(params, target, propertyKey, descriptor);
  createDecorator(function (ctx, key) {
    var options = ctx.options;
    if (!options.emits) {
      options.emits = {};
    }
    var emits = options.emits;
    var emitName = "update:" + key;
    emits[emitName] = null;
  })(target, propertyKey);
}
function Model(target) {
  var propertyKey = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
  var descriptor = arguments.length > 2 ? arguments[2] : undefined;
  if (propertyKey) {
    create$6({}, target, propertyKey, descriptor);
  } else {
    var params = target;
    return function (target, propertyKey, descriptor) {
      create$6(params, target, propertyKey, descriptor);
    };
  }
}
function getDest$2(params, defVal) {
  var dest = params.dest || defVal || Vue.defaultStore || {};
  if (jsUtils.isFunction(dest)) {
    dest = dest();
  }
  return dest;
}
function create$5(params, target, propertyKey, descriptor) {
  createDecorator(function (ctx, key) {
    var options = ctx.options;
    options.methods = options.methods || {};
    var name = params.name || key;
    if (Vue.storeType == 'vuex') {
      options.methods[key] = function () {
        var _this$$store;
        for (var _len4 = arguments.length, args = new Array(_len4), _key4 = 0; _key4 < _len4; _key4++) {
          args[_key4] = arguments[_key4];
        }
        return (_this$$store = this.$store).dispatch.apply(_this$$store, [name].concat(args));
      };
    } else {
      options.methods[key] = function () {
        var dest = getDest$2(params);
        return dest[name].apply(dest, arguments);
      };
    }
  })(target, propertyKey);
}
function Action() {
  for (var _len5 = arguments.length, args = new Array(_len5), _key5 = 0; _key5 < _len5; _key5++) {
    args[_key5] = arguments[_key5];
  }
  if (args.length > 0 && args[0] instanceof Vue) {
    //装饰器
    create$5({
      name: args[1]
    }, args[0], args[1], args[2]);
  } else {
    //装饰器工厂
    return function (target, propertyKey, descriptor) {
      var params = {};
      if (args.length > 0) {
        if (jsUtils.isString(args[0])) {
          params.name = args[0];
        } else {
          params.dest = args[0];
          if (args.length > 1) {
            params.name = args[1];
          }
        }
      }
      if (!params.name) {
        params.name = propertyKey;
      }
      create$5(params, target, propertyKey);
    };
  }
}
function getDest$1(params, defVal) {
  var dest = params.dest || defVal || Vue.defaultStore || {};
  if (jsUtils.isFunction(dest)) {
    dest = dest();
  }
  return dest;
}
function create$4(params, target, propertyKey, descriptor) {
  createDecorator(function (ctx, key) {
    var options = ctx.options;
    options.computed = options.computed || {};
    var name = params.name || key;
    if (Vue.storeType == 'vuex') {
      options.computed[key] = function () {
        var getter = this.$store.getters[name];
        return getter;
      };
    } else {
      options.computed[key] = function () {
        var result = getDest$1(params)[name];
        return result;
      };
    }
  })(target, propertyKey);
}
function Getter() {
  for (var _len6 = arguments.length, args = new Array(_len6), _key6 = 0; _key6 < _len6; _key6++) {
    args[_key6] = arguments[_key6];
  }
  if (args.length > 0 && args[0] instanceof Vue) {
    //装饰器
    create$4({
      name: args[1]
    }, args[0], args[1], args[2]);
  } else {
    //装饰器工厂
    return function (target, propertyKey, descriptor) {
      var params = {};
      if (args.length > 0) {
        if (jsUtils.isString(args[0])) {
          params.name = args[0];
        } else {
          params.dest = args[0];
          if (args.length > 1) {
            params.name = args[1];
          }
        }
      }
      if (!params.name) {
        params.name = propertyKey;
      }
      create$4(params, target, propertyKey);
    };
  }
}
function getDest(params, defVal) {
  var dest = params.dest || defVal || Vue.defaultStore || {};
  if (jsUtils.isFunction(dest)) {
    dest = dest();
  }
  return dest;
}
function create$3(params, target, propertyKey, descriptor) {
  createDecorator(function (ctx, key) {
    var name = params.name || key;
    var arr = name.split(/[/.]/);
    if (Vue.storeType == 'vuex') {
      var options = ctx.options;
      options.computed = options.computed || {};
      options.computed[key] = function () {
        var data = getDest(params, this.$store.state);
        if (!arr) {
          return data[key];
        } else {
          arr.forEach(function (item) {
            data = data[item];
          });
          return data;
        }
      };
    } else {
      ctx.dataProps[key] = {
        get: function get() {
          var data = getDest(params);
          if (!arr) {
            return data[key];
          } else {
            arr.forEach(function (item) {
              data = data[item];
            });
            return data;
          }
        },
        set: function set(v) {
          var data = getDest(params);
          if (!arr) {
            data[key] = v;
          } else {
            var lastIndex = arr.length - 1;
            arr.forEach(function (item, index) {
              if (index == lastIndex) {
                data[item] = v;
              } else {
                data = data[item];
              }
            });
          }
        }
      };
    }
  })(target, propertyKey);
}
function State() {
  for (var _len7 = arguments.length, args = new Array(_len7), _key7 = 0; _key7 < _len7; _key7++) {
    args[_key7] = arguments[_key7];
  }
  if (args.length > 0 && args[0] instanceof Vue) {
    //装饰器
    create$3({
      name: args[1]
    }, args[0], args[1], args[2]);
  } else {
    //装饰器工厂
    return function (target, propertyKey, descriptor) {
      var params = {};
      if (args.length > 0) {
        if (jsUtils.isString(args[0])) {
          params.name = args[0];
        } else {
          params.dest = args[0];
          if (args.length > 1) {
            params.name = args[1];
          }
        }
      }
      if (!params.name) {
        params.name = propertyKey;
      }
      create$3(params, target, propertyKey);
    };
  }
}
function create$2(params, target, propertyKey, descriptor) {
  createDecorator(function (ctx, key) {
    var options = ctx.options;
    options.methods = options.methods || {};
    var dname = params || key;
    options.methods[key] = function () {
      var _this$$store2;
      for (var _len8 = arguments.length, args = new Array(_len8), _key8 = 0; _key8 < _len8; _key8++) {
        args[_key8] = arguments[_key8];
      }
      return (_this$$store2 = this.$store).commit.apply(_this$$store2, [dname].concat(args));
    };
  })(target, propertyKey);
}
function Mutation(target) {
  var propertyKey = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
  var descriptor = arguments.length > 2 ? arguments[2] : undefined;
  if (propertyKey) {
    create$2(propertyKey, target, propertyKey);
  } else {
    var params = target;
    return function (target, propertyKey, descriptor) {
      create$2(params, target, propertyKey);
    };
  }
}
function create$1(params, target, propertyKey, descriptor) {
  createDecorator(function (ctx, key) {
    var options = ctx.options;
    if (!options.inject) {
      options.inject = {};
    }
    var data = Object.assign({
      from: key
    }, params);
    if (jsUtils.isNull(data["default"])) {
      if (descriptor !== null && descriptor !== void 0 && descriptor.value) {
        data["default"] = function () {
          return descriptor.value;
        };
      } else {
        data["default"] = jsUtils.getClassDefaultValue(ctx.classType, key);
      }
    }
    var inject = options.inject;
    inject[key] = data;
    ctx.excludeKeys.push(key);
  })(target, propertyKey);
}
function Inject(target) {
  var propertyKey = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
  var descriptor = arguments.length > 2 ? arguments[2] : undefined;
  if (propertyKey) {
    create$1({
      from: propertyKey
    }, target, propertyKey, descriptor);
  } else {
    var targetType = _typeof(target);
    var params = targetType === 'string' || targetType === 'symbol' ? {
      from: target
    } : target;
    return function (target, propertyKey, descriptor) {
      create$1(params, target, propertyKey, descriptor);
    };
  }
}
function create(params, target, propertyKey, descriptor) {
  createDecorator(function (ctx, key) {
    var options = ctx.options;
    if (!options.provide) {
      var provideFun = function provideFun() {
        var _this5 = this;
        var result = Object.create(null);
        var _iterator = _createForOfIteratorHelper(provideFun.values),
          _step;
        try {
          var _loop = function _loop() {
            var _step$value = _slicedToArray(_step.value, 2),
              key = _step$value[0],
              params = _step$value[1];
            //默认响应式设置
            if (params.ref === false) {
              result[params.name] = _this5[key];
            } else {
              result[params.name] = vue.computed(function () {
                return _this5[key];
              });
            }
          };
          for (_iterator.s(); !(_step = _iterator.n()).done;) {
            _loop();
          }
        } catch (err) {
          _iterator.e(err);
        } finally {
          _iterator.f();
        }
        var _iterator2 = _createForOfIteratorHelper(provideFun.funs),
          _step2;
        try {
          var _loop2 = function _loop2() {
            var _step2$value = _slicedToArray(_step2.value, 2),
              key = _step2$value[0],
              params = _step2$value[1];
            result[params.name] = function () {
              return _this5[key].apply(_this5, arguments);
            };
          };
          for (_iterator2.s(); !(_step2 = _iterator2.n()).done;) {
            _loop2();
          }
        } catch (err) {
          _iterator2.e(err);
        } finally {
          _iterator2.f();
        }
        return result;
      };
      provideFun.values = new Map();
      provideFun.funs = new Map();
      options.provide = provideFun;
    }
    var provide = options.provide;
    if (descriptor !== null && descriptor !== void 0 && descriptor.value) {
      provide.funs.set(key, params);
    } else if (descriptor !== null && descriptor !== void 0 && descriptor.get) {
      provide.values.set(key, params);
    } else {
      provide.values.set(key, params);
    }
  })(target, propertyKey);
}
function Provide(target) {
  var propertyKey = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : "";
  var descriptor = arguments.length > 2 ? arguments[2] : undefined;
  if (propertyKey) {
    create({
      name: propertyKey
    }, target, propertyKey, descriptor);
  } else {
    var targetType = _typeof(target);
    var params = targetType === 'string' || targetType === 'symbol' ? {
      name: target
    } : target;
    return function (target, propertyKey, descriptor) {
      create(params, target, propertyKey, descriptor);
    };
  }
}
exports.Action = Action;
exports.Getter = Getter;
exports.Inject = Inject;
exports.Model = Model;
exports.Mutation = Mutation;
exports.Options = Options;
exports.Prop = Prop;
exports.Provide = Provide;
exports.State = State;
exports.Vue = Vue;
exports.Watch = Watch;
exports.WrapMode = WrapMode;
exports.createDecorator = createDecorator;
exports["default"] = vuePlugin;
exports.getOptionsByClass = getOptionsByClass;
exports.mergeSetup = mergeSetup;
exports.mixins = mixins;
exports.setDefaultOptions = setDefaultOptions;
