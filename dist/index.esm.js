/**
  * gowiny-vue-class v1.1.6
  * https://gitee.com/gowiny/vue-class
  *
  * (c) 2022-present gowiny
  * @license MIT
  *
  * Date: 2024-12-05T12:52:44Z
  */
import { isFunction, isNull, clone, getClassDefaultValue, isString } from '@gowiny/js-utils';
import { ref, shallowReadonly, shallowReactive, shallowRef, computed } from 'vue';

function getOptionsByClass(Ctor) {
    let options;
    if (Ctor.__vccOpts) {
        options = Ctor.__vccOpts;
    }
    else {
        options = Ctor;
    }
    return options;
}
function toEmitMap(val) {
    if (!val || !Array.isArray(val)) {
        return val;
    }
    else {
        let rs = {};
        val.forEach(item => {
            rs[item] = null;
        });
        return rs;
    }
}
function mergeSetup(...args) {
    if (!args) {
        return;
    }
    else if (args.length == 1) {
        return args[0];
    }
    else {
        const result = {};
        args.forEach((item) => {
            if (!item) {
                return;
            }
            else if (isFunction(item)) {
                result.render = item;
            }
            else {
                Object.assign(result, item);
            }
        });
        return result;
    }
}
function initDecorator(Ctor) {
    if (!Ctor.__d) {
        Ctor.__d = [];
        Ctor.__d.__ctor = Ctor;
    }
    else if (Ctor.__d.__ctor !== Ctor) {
        Ctor.__d = [].concat(Ctor.__d);
        Ctor.__d.__ctor = Ctor;
    }
}
function createDecorator(factory) {
    return (target, key, index) => {
        const Ctor = typeof target === 'function'
            ? target
            : target.constructor;
        initDecorator(Ctor);
        if (typeof index !== 'number') {
            index = undefined;
        }
        Ctor.__d.push((ctx) => factory(ctx, key, index));
    };
}
function mixins(...Ctors) {
    return class MixedVue extends Vue {
        static __b = {
            mixins: Ctors.map((Ctor) => Ctor.__vccOpts),
        };
        constructor(...args) {
            super(...args);
            Ctors.forEach((Ctor) => {
                const data = new Ctor(...args);
                Object.keys(data).forEach((key) => {
                    this[key] = data[key];
                });
            });
        }
    };
}

const EXCLUDE_SUPER_PROPS = ['data', 'setup'];
function defineRawProxy(proxy, key, target) {
    Object.defineProperty(proxy, key, {
        get: () => target[key],
        set: (value) => {
            target[key] = value;
        },
        enumerable: true,
        configurable: true,
    });
}
function defineRefProxy(proxy, key, target) {
    Object.defineProperty(proxy, key, {
        get: () => target[key].value,
        set: (value) => {
            target[key].value = value;
        },
        enumerable: true,
        configurable: true,
    });
}
function getSuper(Ctor) {
    const superProto = Object.getPrototypeOf(Ctor.prototype);
    if (!superProto) {
        return undefined;
    }
    return superProto.constructor;
}
function getOwn(value, key) {
    return value.hasOwnProperty(key) ? value[key] : undefined;
}
function copyNotExistProps(dest, src, propNames) {
    dest = dest || {};
    if (src) {
        const destPropNames = Object.getOwnPropertyNames(dest);
        propNames = propNames || Object.getOwnPropertyNames(src);
        if (propNames.length > 0) {
            propNames.forEach(item => {
                if (destPropNames.indexOf(item) == -1) {
                    dest[item] = src[item];
                }
            });
        }
    }
    return dest;
}
function getNotExistProps(src, dest, propNames) {
    let result;
    if (src) {
        propNames = propNames || Object.getOwnPropertyNames(src);
        if (propNames.length > 0) {
            const isNullDest = isNull(dest);
            result = {};
            propNames.forEach(item => {
                const srcVal = src[item];
                if (!isNull(srcVal) && (isNullDest || isNull(dest[item]))) {
                    result[item] = srcVal;
                }
            });
        }
        result = result || { ...src };
    }
    return result;
}
const defaultBeforeFactoryHandle = function (ctx) {
    const options = ctx.options;
    const Ctor = ctx.classType;
    const proto = Ctor.prototype;
    const propNames = Object.getOwnPropertyNames(proto);
    // Inject base options as a mixin
    const base = getOwn(Ctor, '__b');
    if (base) {
        options.mixins = options.mixins || [];
        options.mixins.unshift(base);
    }
    options.methods = { ...options.methods };
    options.computed = { ...options.computed };
    propNames.forEach((key) => {
        if (key === 'constructor') {
            return;
        }
        // hooks
        if (Ctor.__h.indexOf(key) > -1) {
            options[key] = proto[key];
            return;
        }
        const descriptor = Object.getOwnPropertyDescriptor(proto, key);
        if (key === 'setup') {
            options.setup = descriptor.value;
            return;
        }
        // methods
        if (typeof descriptor.value === 'function') {
            options.methods[key] = descriptor.value;
            return;
        }
        // computed properties
        if (descriptor.get || descriptor.set) {
            options.computed[key] = {
                get: descriptor.get,
                set: descriptor.set,
            };
            return;
        }
    });
    // from Vue Loader
    const injections = [
        'render',
        'ssrRender',
        '__file',
        '__cssModules',
        '__scopeId',
        '__hmrId',
    ];
    injections.forEach((key) => {
        if (Ctor[key]) {
            options[key] = Ctor[key];
        }
    });
    // Handle super class options
    const Super = getSuper(Ctor);
    if (Super) {
        const superOptions = Super.__vccOpts;
        const extendOptions = getNotExistProps(superOptions, options, Ctor.__h);
        if (!options.components) {
            options.components = {};
        }
        options.components = copyNotExistProps(options.components, superOptions.components);
        options.computed = copyNotExistProps(options.computed, superOptions.computed);
        options.methods = copyNotExistProps(options.methods, superOptions.methods);
        options.watch = copyNotExistProps(options.watch, superOptions.watch);
        options.emits = copyNotExistProps(toEmitMap(options.emits), toEmitMap(superOptions.emits));
        EXCLUDE_SUPER_PROPS.forEach(item => {
            delete extendOptions[item];
        });
        Object.assign(options, extendOptions);
        //options.extends = extendOptions
    }
    return options;
};
const defaultDataFactory = function (ctx) {
    const result = function () {
        const Ctor = ctx.classType;
        const excludeProps = ctx.excludeProps;
        const data = new Ctor();
        const plainData = {};
        const dataKeys = Object.keys(data);
        dataKeys.forEach((key) => {
            let val;
            if (excludeProps.indexOf(key) > -1 || ((val = data[key]) && val.__s)) {
                return;
            }
            const dataOption = ctx.dataPropOptions[key];
            const wrapMode = dataOption && dataOption.wrapMode ? dataOption.wrapMode : 'ref';
            switch (wrapMode) {
                case 'ref':
                    plainData[key] = ref(val);
                    defineRefProxy(data, key, plainData);
                    break;
                case 'raw':
                    plainData[key] = val;
                    defineRawProxy(data, key, plainData);
                    break;
                case 'shallowRef':
                    plainData[key] = shallowRef(val);
                    defineRefProxy(data, key, plainData);
                    break;
                case 'shallowReactive':
                    plainData[key] = shallowReactive(val);
                    defineRawProxy(data, key, plainData);
                    break;
                case 'shallowReadonly':
                    plainData[key] = shallowReadonly(val);
                    defineRawProxy(data, key, plainData);
                    break;
                default:
                    plainData[key] = ref(val);
                    defineRefProxy(data, key, plainData);
                    break;
            }
        });
        Object.defineProperties(plainData, ctx.dataProps);
        return plainData;
    };
    return result;
};
const defaultAfterFactoryHandle = function (ctx) {
    const options = ctx.options;
    const excludeKeys = [...ctx.excludeKeys];
    if (options.computed) {
        excludeKeys.push(...Object.keys(options.computed));
    }
    if (options.props) {
        excludeKeys.push(...Object.keys(options.props));
    }
    const excludeMethods = [...excludeKeys, ...ctx.excludeMethods];
    excludeMethods.forEach((key) => {
        delete options.methods[key];
    });
    if (options.methods) {
        excludeKeys.push(...Object.keys(options.methods));
    }
    const excludeProps = [...excludeKeys, ...ctx.excludeProps];
    ctx.excludeProps = excludeProps;
    return options;
};
class OptionsContextImpl {
    options;
    classType;
    dataFactory;
    beforeHandle;
    afterHandle;
    attrs = new Map();
    factoryMap = {};
    excludeKeys = [];
    excludeMethods = [];
    excludeProps = [];
    dataProps = {};
    dataPropOptions = {};
    constructor(options, target) {
        this.options = options;
        this.classType = target;
        this.dataFactory = defaultDataFactory;
        this.beforeHandle = defaultBeforeFactoryHandle;
        this.afterHandle = defaultAfterFactoryHandle;
    }
    addFactory(factory) {
        this.factoryMap[factory.name] = factory;
    }
    removeFactory(name) {
        delete this.factoryMap[name];
    }
    hasFactory(name) {
        return Reflect.has(this.factoryMap, name);
    }
    getOrderedFactories() {
        let list = Object.values(this.factoryMap);
        list = list.sort((o1, o2) => {
            return o1.order - o2.order;
        });
        return list;
    }
    clearFactories() {
        const keys = Object.keys(this.factoryMap);
        keys.forEach(key => {
            delete this.factoryMap[key];
        });
    }
    getAttr(name) {
        return this.attrs.get(name);
    }
    hasAttr(name) {
        return this.attrs.has(name);
    }
    setAttr(name, value) {
        this.attrs.set(name, value);
    }
    removeAttr(name) {
        this.attrs.delete(name);
    }
    clearAttrs() {
        this.attrs.clear();
    }
}
class VueImpl {
    static __h = [
        'beforeCreate',
        'created',
        'beforeMount',
        'mounted',
        'beforeUnmount',
        'unmounted',
        'beforeUpdate',
        'updated',
        'activated',
        'deactivated',
        'render',
        'errorCaptured',
        'serverPrefetch',
    ];
    static storeType = 'vuex';
    static defaultStore;
    static get __vccOpts() {
        // Early return if `this` is base class as it does not have any options
        if (this === Vue) {
            return {};
        }
        const Ctor = this;
        const cache = getOwn(Ctor, '__c');
        if (cache) {
            return cache;
        }
        initDecorator(Ctor);
        // If the options are provided via decorator use it as a base
        let options = { ...getOwn(Ctor, '__o') };
        const optionsContext = new OptionsContextImpl(options, Ctor);
        const decorators = getOwn(Ctor, '__d');
        if (decorators) {
            decorators.forEach((fn) => fn(optionsContext));
        }
        if (optionsContext.beforeHandle) {
            options = optionsContext.beforeHandle(optionsContext);
            optionsContext.options = options;
        }
        const factories = optionsContext.getOrderedFactories();
        for (let i = 0; i < factories.length; i++) {
            const item = factories[i];
            options = item.handle(optionsContext);
            optionsContext.options = options;
        }
        if (optionsContext.afterHandle) {
            options = optionsContext.afterHandle(optionsContext);
            optionsContext.options = options;
        }
        const dataFactory = optionsContext.dataFactory || defaultDataFactory;
        options.data = dataFactory(optionsContext);
        if (!options.name) {
            options.name = Ctor.name;
        }
        Ctor.__c = options;
        return options;
    }
    static registerHooks(keys) {
        keys.forEach(item => {
            if (this.__h.indexOf(item) == -1) {
                this.__h.push(item);
            }
        });
    }
    static with(Props) {
        const propsMeta = new Props();
        const props = {};
        Object.keys(propsMeta).forEach((key) => {
            const meta = propsMeta[key];
            props[key] = meta ?? null;
        });
        class PropsMixin extends this {
            static __b = {
                props,
            };
        }
        return PropsMixin;
    }
}
const Vue = VueImpl;

function setDefaultOptions(options) {
    const hooks = options.hooks;
    Vue.storeType = options.storeType || 'vuex';
    Vue.defaultStore = options.defaultStore;
    if (hooks) {
        Vue.registerHooks(hooks);
    }
}
var vuePlugin = {
    install: (app, options, ...args) => {
        app.config.unwrapInjectedRef = true;
        setDefaultOptions(options);
    }
};

function Options(conf) {
    if (typeof (conf) === 'function') {
        conf.__o = {};
        return conf;
    }
    else {
        return (Component) => {
            Component.__o = conf;
            return Component;
        };
    }
}

function create$8(params, wopt, target, propertyKey, descriptor) {
    createDecorator(function (ctx, key) {
        const options = ctx.options;
        if (!options.watch) {
            options.watch = {};
        }
        const name = params || key;
        const watch = options.watch;
        let watchOption;
        if (wopt) {
            watchOption = Object.assign({}, wopt, {
                handler: descriptor?.value
            });
        }
        else {
            watchOption = {
                handler: descriptor?.value
            };
        }
        if (Array.isArray(name)) {
            name.forEach((item) => {
                watch[item] = watchOption;
            });
        }
        else {
            watch[name] = watchOption;
        }
        ctx.excludeKeys.push(key);
    })(target, propertyKey);
}
function Watch(target, propertyKey = "", descriptor) {
    if (propertyKey && typeof (propertyKey) === 'string') {
        create$8(propertyKey, undefined, target, propertyKey, descriptor);
    }
    else {
        const params = target;
        const watchOptions = propertyKey;
        return function (target, propertyKey, descriptor) {
            create$8(params, watchOptions, target, propertyKey, descriptor);
        };
    }
}

function WrapMode(wrapMode) {
    return function (target, propertyKey) {
        createDecorator(function (ctx, key) {
            let conf = ctx.dataPropOptions[key];
            if (!conf) {
                conf = { wrapMode };
                ctx.dataPropOptions[key] = conf;
            }
            else {
                conf.wrapMode = wrapMode;
            }
        })(target, propertyKey);
    };
}

function create$7(params, target, propertyKey, descriptor) {
    createDecorator(function (ctx, key) {
        const options = ctx.options;
        if (!options.props) {
            options.props = {};
        }
        const props = options.props;
        const oldDefault = params.default;
        const defType = typeof (oldDefault);
        if (descriptor) { //定义在方法上
            if (defType === 'undefined') {
                params.default = function () { return descriptor.value; };
            }
        }
        else { //定义在属性上
            if (defType === 'object') {
                params.default = clone(oldDefault);
            }
            else if (defType === 'undefined') {
                params.default = getClassDefaultValue(ctx.classType, key);
            }
        }
        props[key] = params;
    })(target, propertyKey);
}
function Prop(target, propertyKey = "", descriptor) {
    if (propertyKey) {
        create$7({}, target, propertyKey, descriptor);
    }
    else {
        const params = target;
        return function (target, propertyKey, descriptor) {
            create$7(params, target, propertyKey, descriptor);
        };
    }
}

function create$6(params, target, propertyKey, descriptor) {
    create$7(params, target, propertyKey, descriptor);
    createDecorator(function (ctx, key) {
        const options = ctx.options;
        if (!options.emits) {
            options.emits = {};
        }
        const emits = options.emits;
        const emitName = "update:" + key;
        emits[emitName] = null;
    })(target, propertyKey);
}
function Model(target, propertyKey = "", descriptor) {
    if (propertyKey) {
        create$6({}, target, propertyKey, descriptor);
    }
    else {
        const params = target;
        return function (target, propertyKey, descriptor) {
            create$6(params, target, propertyKey, descriptor);
        };
    }
}

function getDest$2(params, defVal) {
    let dest = params.dest || defVal || Vue.defaultStore || {};
    if (isFunction(dest)) {
        dest = dest();
    }
    return dest;
}
function create$5(params, target, propertyKey, descriptor) {
    createDecorator(function (ctx, key) {
        const options = ctx.options;
        options.methods = options.methods || {};
        const name = params.name || key;
        if (Vue.storeType == 'vuex') {
            options.methods[key] = function (...args) {
                return this.$store.dispatch(name, ...args);
            };
        }
        else {
            options.methods[key] = function (...args) {
                const dest = getDest$2(params);
                return dest[name](...args);
            };
        }
    })(target, propertyKey);
}
function Action(...args) {
    if (args.length > 0 && args[0] instanceof Vue) { //装饰器
        create$5({
            name: args[1]
        }, args[0], args[1], args[2]);
    }
    else { //装饰器工厂
        return function (target, propertyKey, descriptor) {
            const params = {};
            if (args.length > 0) {
                if (isString(args[0])) {
                    params.name = args[0];
                }
                else {
                    params.dest = args[0];
                    if (args.length > 1) {
                        params.name = args[1];
                    }
                }
            }
            if (!params.name) {
                params.name = propertyKey;
            }
            create$5(params, target, propertyKey);
        };
    }
}

function getDest$1(params, defVal) {
    let dest = params.dest || defVal || Vue.defaultStore || {};
    if (isFunction(dest)) {
        dest = dest();
    }
    return dest;
}
function create$4(params, target, propertyKey, descriptor) {
    createDecorator(function (ctx, key) {
        const options = ctx.options;
        options.computed = options.computed || {};
        const name = params.name || key;
        if (Vue.storeType == 'vuex') {
            options.computed[key] = function () {
                const getter = this.$store.getters[name];
                return getter;
            };
        }
        else {
            options.computed[key] = function () {
                const result = getDest$1(params)[name];
                return result;
            };
        }
    })(target, propertyKey);
}
function Getter(...args) {
    if (args.length > 0 && args[0] instanceof Vue) { //装饰器
        create$4({
            name: args[1]
        }, args[0], args[1], args[2]);
    }
    else { //装饰器工厂
        return function (target, propertyKey, descriptor) {
            const params = {};
            if (args.length > 0) {
                if (isString(args[0])) {
                    params.name = args[0];
                }
                else {
                    params.dest = args[0];
                    if (args.length > 1) {
                        params.name = args[1];
                    }
                }
            }
            if (!params.name) {
                params.name = propertyKey;
            }
            create$4(params, target, propertyKey);
        };
    }
}

function getDest(params, defVal) {
    let dest = params.dest || defVal || Vue.defaultStore || {};
    if (isFunction(dest)) {
        dest = dest();
    }
    return dest;
}
function create$3(params, target, propertyKey, descriptor) {
    createDecorator(function (ctx, key) {
        const name = params.name || key;
        const arr = name.split(/[/.]/);
        if (Vue.storeType == 'vuex') {
            const options = ctx.options;
            options.computed = options.computed || {};
            options.computed[key] = function () {
                let data = getDest(params, this.$store.state);
                if (!arr) {
                    return data[key];
                }
                else {
                    arr.forEach((item) => {
                        data = data[item];
                    });
                    return data;
                }
            };
        }
        else {
            ctx.dataProps[key] = {
                get() {
                    let data = getDest(params);
                    if (!arr) {
                        return data[key];
                    }
                    else {
                        arr.forEach((item) => {
                            data = data[item];
                        });
                        return data;
                    }
                },
                set(v) {
                    let data = getDest(params);
                    if (!arr) {
                        data[key] = v;
                    }
                    else {
                        const lastIndex = arr.length - 1;
                        arr.forEach((item, index) => {
                            if (index == lastIndex) {
                                data[item] = v;
                            }
                            else {
                                data = data[item];
                            }
                        });
                    }
                }
            };
        }
    })(target, propertyKey);
}
function State(...args) {
    if (args.length > 0 && args[0] instanceof Vue) { //装饰器
        create$3({
            name: args[1]
        }, args[0], args[1], args[2]);
    }
    else { //装饰器工厂
        return function (target, propertyKey, descriptor) {
            const params = {};
            if (args.length > 0) {
                if (isString(args[0])) {
                    params.name = args[0];
                }
                else {
                    params.dest = args[0];
                    if (args.length > 1) {
                        params.name = args[1];
                    }
                }
            }
            if (!params.name) {
                params.name = propertyKey;
            }
            create$3(params, target, propertyKey);
        };
    }
}

function create$2(params, target, propertyKey, descriptor) {
    createDecorator(function (ctx, key) {
        const options = ctx.options;
        options.methods = options.methods || {};
        const dname = params || key;
        options.methods[key] = function (...args) {
            return this.$store.commit(dname, ...args);
        };
    })(target, propertyKey);
}
function Mutation(target, propertyKey = "", descriptor) {
    if (propertyKey) {
        create$2(propertyKey, target, propertyKey);
    }
    else {
        const params = target;
        return function (target, propertyKey, descriptor) {
            create$2(params, target, propertyKey);
        };
    }
}

function create$1(params, target, propertyKey, descriptor) {
    createDecorator(function (ctx, key) {
        const options = ctx.options;
        if (!options.inject) {
            options.inject = {};
        }
        const data = Object.assign({ from: key }, params);
        if (isNull(data.default)) {
            if (descriptor?.value) {
                data.default = function () { return descriptor.value; };
            }
            else {
                data.default = getClassDefaultValue(ctx.classType, key);
            }
        }
        const inject = options.inject;
        inject[key] = data;
        ctx.excludeKeys.push(key);
    })(target, propertyKey);
}
function Inject(target, propertyKey = "", descriptor) {
    if (propertyKey) {
        create$1({ from: propertyKey }, target, propertyKey, descriptor);
    }
    else {
        const targetType = typeof target;
        const params = targetType === 'string' || targetType === 'symbol' ? { from: target } : target;
        return function (target, propertyKey, descriptor) {
            create$1(params, target, propertyKey, descriptor);
        };
    }
}

function create(params, target, propertyKey, descriptor) {
    createDecorator(function (ctx, key) {
        const options = ctx.options;
        if (!options.provide) {
            const provideFun = function () {
                const result = Object.create(null);
                for (const [key, params] of provideFun.values) {
                    //默认响应式设置
                    if (params.ref === false) {
                        result[params.name] = this[key];
                    }
                    else {
                        result[params.name] = computed(() => this[key]);
                    }
                }
                for (const [key, params] of provideFun.funs) {
                    result[params.name] = (...args) => {
                        return this[key](...args);
                    };
                }
                return result;
            };
            provideFun.values = new Map();
            provideFun.funs = new Map();
            options.provide = provideFun;
        }
        const provide = options.provide;
        if (descriptor?.value) {
            provide.funs.set(key, params);
        }
        else if (descriptor?.get) {
            provide.values.set(key, params);
        }
        else {
            provide.values.set(key, params);
        }
    })(target, propertyKey);
}
function Provide(target, propertyKey = "", descriptor) {
    if (propertyKey) {
        create({ name: propertyKey }, target, propertyKey, descriptor);
    }
    else {
        const targetType = typeof target;
        const params = targetType === 'string' || targetType === 'symbol' ? { name: target } : target;
        return function (target, propertyKey, descriptor) {
            create(params, target, propertyKey, descriptor);
        };
    }
}

export { Action, Getter, Inject, Model, Mutation, Options, Prop, Provide, State, Vue, Watch, WrapMode, createDecorator, vuePlugin as default, getOptionsByClass, mergeSetup, mixins, setDefaultOptions };
