## [1.1.5](https://gitee.com/gowiny/vue-class/compare/v1.1.3...v1.1.5) (2024-05-22)

解决BUG

## [1.1.3](https://gitee.com/gowiny/vue-class/compare/v1.1.2...v1.1.3) (2023-01-16)

增加 setDefaultOptions 函数

## [1.1.2](https://gitee.com/gowiny/vue-class/compare/v1.1.1...v1.1.2) (2023-01-06)

解决 方法 跟 属性 冲突的问题

## [1.1.1](https://gitee.com/gowiny/vue-class/compare/v1.1.0...v1.1.1) (2023-01-06)

解决类属性初始值为 undefined 的时候控制台报警的问题

# [1.1.0](https://gitee.com/gowiny/vue-class/compare/v1.0.68...v1.1.0) (2023-01-05)

把@RefType 改名为 @WrapMode

## [1.0.68](https://gitee.com/gowiny/vue-class/compare/v1.0.67...v1.0.68) (2023-01-05)

解决@RefType的BUG

## [1.0.67](https://gitee.com/gowiny/vue-class/compare/v1.0.66...v1.0.67) (2023-01-05)

删除 @MarkRaw ，增加@RefType(refType:'ref' | 'raw' | 'shallowRef' | 'shallowReactive' | 'shallowReadonly')  
refType分别对应Vue里的ref，markRaw，shallowRef，shallowReactive，shallowReadonly

## [1.0.66](https://gitee.com/gowiny/vue-class/compare/v1.0.65...v1.0.66) (2023-01-05)

导出 DataPropOptions 类型

## [1.0.65](https://gitee.com/gowiny/vue-class/compare/v1.0.64...v1.0.65) (2023-01-05)

增加@MarkRaw装饰器，等同于Vue里的 markRaw 函数，
将一个对象标记为不可被转为代理。返回该对象本身。

## [1.0.64](https://gitee.com/gowiny/vue-class/compare/v1.0.63...v1.0.64) (2023-01-04)

增加对Pinia的支持
```javascript
app.use(vueClass,{
    storeType:'pinia',//设定store类型位Pinia
    defaultStore:useAppStore,//设置默认的Store
    hooks:['beforeRouteEnter','beforeRouteUpdate','beforeRouteLeave']
  })
```

## [1.0.63](https://gitee.com/gowiny/vue-class/compare/v1.0.62...v1.0.63) (2022-03-26)

修改说明文件

## [1.0.62](https://gitee.com/gowiny/vue-class/compare/v1.0.61...v1.0.62) (2022-03-26)

把自定义Decorator的 createDecorator 方法导出

## [1.0.61](https://gitee.com/gowiny/vue-class/compare/v1.0.60...v1.0.61) (2022-03-26)



## [1.0.60](https://gitee.com/gowiny/vue-class/compare/v1.0.59...v1.0.60) (2022-03-25)

解决了一个BUG

## [1.0.59](https://gitee.com/gowiny/vue-class/compare/v1.0.58...v1.0.59) (2022-03-24)

### Features
* 不再依赖 vue-class-component
觉得它的实现方式，跟正常的Class 的用法不一致，所以直接重新改写了。
vue-class-component 的用法，如果子类也写有生命周期的hook，那实际执行的是 所有父类的hook 和 子类的 hook 都会执行一次。
改写后，子类重写父类方法后，如果不显式调用super.xxx 的话，是不会执行父类的hook 的。这跟正常的类的继承、重写保持一致

* 增加 getOptionsByClass 方法，可以让 <script setup lang="ts"></script> 和 <script lang="ts"></script> 混用
* 支持uniapp 里使用 Class方式定义组件。



```javascript
<script setup lang="ts">
  const name = 'name'
</script>
<script lang="ts">
import { Watch,Prop,Vue,getOptionsByClass} from '@gowiny/vue-class'
class MyTest extends Vue{

  @Prop
  readonly username!:string
  title='hello'

  @Watch("title")
  watchTitleChange(val:any){
    console.log('title is changed,',val)
  }

}
export default getOptionsByClass(MyTest)
</script>
```


