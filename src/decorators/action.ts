import { createDecorator } from '../core/helpers'
import { isFunction, isString } from '@gowiny/js-utils';
import { Vue } from '../core/vue';

interface Params{
    name?:string,
    dest?:any
}

function getDest(params:Params,defVal?:any){
    let dest = params.dest || defVal || Vue.defaultStore || {};
    if(isFunction(dest)){
        dest = dest();
    }
    return dest;
}


function create(params:Params,target: any, propertyKey: string,descriptor?: PropertyDescriptor){
    createDecorator(function (ctx, key) {
        const options = ctx.options
        options.methods = options.methods || {}
        const name = params.name || key;
        if(Vue.storeType == 'vuex'){
            options.methods[key] = function(...args:any[]) {
                return this.$store.dispatch(name,...args)
            }
        }else{
            options.methods[key] = function(...args:any[]) {
                const dest = getDest(params);
                return dest[name](...args);
            }
        }
        
    })(target, propertyKey);
}

export function Action(params:string):any;
export function Action(dest:any,params?:string):any;
export function Action(target: any, propertyKey: string):void;
export function Action(target: any, propertyKey: string,descriptor: PropertyDescriptor):void;
export function Action(...args:any[]):void | any{
    if(args.length > 0 && args[0] instanceof Vue){//装饰器
        create({
            name:args[1]
        },args[0],args[1],args[2])
    }else{//装饰器工厂
        return function (target: any, propertyKey: string,descriptor?: PropertyDescriptor) {

            const params:Params = {

            };
            if(args.length > 0){
                if(isString(args[0])){
                    params.name = args[0];
                }else{
                    params.dest = args[0];
                    if(args.length > 1){
                        params.name = args[1];
                    }
                }
            }
            if(!params.name){
                params.name = propertyKey
            }
            create(params,target,propertyKey,descriptor)
        };
    }
}
