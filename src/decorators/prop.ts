import { createDecorator } from '../core/helpers'
import { clone, getClassDefaultValue } from '@gowiny/js-utils';
import { PropOptions } from '../core/vue'

export function create(params:PropOptions,target: any, propertyKey: string,descriptor?: PropertyDescriptor){
    createDecorator(function (ctx, key) {
        const options = ctx.options
        if(!options.props){
            options.props = {}
        }
        const props = options.props
        const oldDefault = params.default
        const defType = typeof(oldDefault)
        if(descriptor){//定义在方法上
            if(defType === 'undefined'){
                params.default = function(){ return descriptor.value }
            }
        }else{//定义在属性上
            if(defType === 'object'){
                params.default = clone(oldDefault)
            }else if(defType === 'undefined'){
                params.default = getClassDefaultValue(ctx.classType,key)
            }
        }
        props[key] = params
    })(target, propertyKey);
}

export function Prop(params:PropOptions):any;
export function Prop(target: any, propertyKey: string):void;
export function Prop(target: any, propertyKey: string,descriptor: PropertyDescriptor):void;
export function Prop(target: any, propertyKey="",descriptor?: PropertyDescriptor):void | any{
    if(propertyKey){
        create({},target,propertyKey,descriptor)
    }else{
        const params:PropOptions = target;
        return function (target: any, propertyKey: string,descriptor?: PropertyDescriptor) {
            create(params,target,propertyKey,descriptor)
        };

    }
}
