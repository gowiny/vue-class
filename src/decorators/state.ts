import { createDecorator } from '../core/helpers'
import { isFunction, isString } from '@gowiny/js-utils';
import { Vue } from '../core/vue';

interface Params{
    name?:string,
    dest?:any
}

function getDest(params:Params,defVal?:any){
    let dest = params.dest || defVal || Vue.defaultStore || {};
    if(isFunction(dest)){
        dest = dest();
    }
    return dest;
}

function create(params:Params,target: any, propertyKey: string,descriptor?: PropertyDescriptor){
    createDecorator(function (ctx, key) {
        const name = params.name || key;
        const arr = name.split(/[/.]/);
        if(Vue.storeType == 'vuex'){
            const options = ctx.options
            options.computed = options.computed || {}
            options.computed[key] = function() {
                let data:any = getDest(params,this.$store.state);
                if(!arr){
                    return data[key];
                }else{
                    arr.forEach((item)=>{
                        data = data[item]
                    })
                    return data;
                }
            }
        }else{
            ctx.dataProps[key] = {
                get(){
                    let data:any = getDest(params);
                    if(!arr){
                        return data[key];
                    }else{
                        arr.forEach((item)=>{
                            data = data[item]
                        })
                        return data;
                    }
                },
                set(v:any){
                    let data:any = getDest(params);
                    if(!arr){
                        data[key] = v;
                    }else{
                        const lastIndex = arr.length - 1;
                        arr.forEach((item,index)=>{
                            if(index == lastIndex){
                                data[item] = v;
                            }else{
                                data = data[item]
                            }
                        })
                    }
                }
            }
        }
        
    })(target, propertyKey);
}

export function State(params:string):any;
export function State(dest:any,params?:string):any;
export function State(target: any, propertyKey: string):void;
export function State(target: any, propertyKey: string,descriptor: PropertyDescriptor):void;
export function State(...args:any[]):void | any{
    if(args.length > 0 && args[0] instanceof Vue){//装饰器
        create({
            name:args[1]
        },args[0],args[1],args[2])
    }else{//装饰器工厂
        return function (target: any, propertyKey: string,descriptor?: PropertyDescriptor) {

            const params:Params = {

            };
            if(args.length > 0){
                if(isString(args[0])){
                    params.name = args[0];
                }else{
                    params.dest = args[0];
                    if(args.length > 1){
                        params.name = args[1];
                    }
                }
            }
            if(!params.name){
                params.name = propertyKey
            }
            create(params,target,propertyKey,descriptor)
        };
    }
    
}
