import { createDecorator } from '../core/helpers'
import { DataPropWrapMode } from '../core/vue';

export function WrapMode(wrapMode:DataPropWrapMode):any{
    return function (target: any, propertyKey: string) {
        createDecorator(function (ctx, key) {
            let conf = ctx.dataPropOptions[key];
            if(!conf){
                conf = {wrapMode};
                ctx.dataPropOptions[key] = conf;
            }else{
                conf.wrapMode = wrapMode;
            }
        })(target, propertyKey);
    }
}
