import { createDecorator } from '../core/helpers'

function create(params:string,target: any, propertyKey: string,descriptor?: PropertyDescriptor){
    createDecorator(function (ctx, key) {
        const options = ctx.options
        options.methods = options.methods || {}
        const dname = params || key;
        options.methods[key] = function(...args:any[]) {
            return this.$store.commit(dname,...args)
        }
    })(target, propertyKey);
}

export function Mutation(params:string):any;
export function Mutation(target: any, propertyKey: string,descriptor: PropertyDescriptor):void;
export function Mutation(target: any, propertyKey="",descriptor?: PropertyDescriptor):void | any{
    if(propertyKey){
        create(propertyKey,target,propertyKey,descriptor)
    }else{
        const params:string = target;
        return function (target: any, propertyKey: string,descriptor?: PropertyDescriptor) {
            create(params,target,propertyKey,descriptor)
        };

    }
}
