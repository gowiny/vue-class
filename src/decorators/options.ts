import { ComponentOptions} from "vue"
import { Vue,VueConstructor, VueBase } from "../core/vue"
/*
export function Options<V extends Vue>(options: ComponentOptions & ThisType<V>): <VC extends VueConstructor<VueBase>>(target: VC) => VC {
    return (Component) => {
        Component.__o = options
        return Component
    }
}
*/


export function Options<V extends Vue>(constructor:V): <VC extends VueConstructor<VueBase>>(target: VC) => VC;
export function Options<V extends Vue>(options: ComponentOptions & ThisType<V>): <VC extends VueConstructor<VueBase>>(target: VC) => VC ;
export function Options<V extends Vue>(conf:any): <VC extends VueConstructor<VueBase>>(target: VC) => VC {
    if(typeof(conf) === 'function'){
        conf.__o = {};
        return conf;
    }else{
        return (Component) => {
            Component.__o = conf
            return Component
        }
    }
    
}