import { createDecorator } from '../core/helpers'
import { PropOptions } from '../core/vue'
import { create as createProp } from './prop'
function create(params:PropOptions,target: any, propertyKey: string,descriptor?: PropertyDescriptor){
    createProp(params,target,propertyKey,descriptor);
    createDecorator(function (ctx, key) {
        const options = ctx.options
        if(!options.emits){
            options.emits = {}
        }
        const emits = options.emits
        const emitName = "update:"+key;
        emits[emitName] = null
    })(target, propertyKey);
}

export function Model(params:PropOptions):any;
export function Model(target: any, propertyKey: string):void;
export function Model(target: any, propertyKey: string,descriptor: PropertyDescriptor):void;
export function Model(target: any, propertyKey="",descriptor?: PropertyDescriptor):void | any{
    if(propertyKey){
        create({},target,propertyKey,descriptor)
    }else{
        const params:PropOptions = target;
        return function (target: any, propertyKey: string,descriptor?: PropertyDescriptor) {
            create(params,target,propertyKey,descriptor)
        };

    }
}
