import {Vue} from './vue'
import {App} from 'vue'

export interface VueClassConfig{
    hooks?:string[],
    storeType?:"vuex" | "pinia",
    defaultStore?:any
}

export function setDefaultOptions(options:VueClassConfig){
    const hooks = options.hooks
    Vue.storeType = options.storeType || 'vuex';
    Vue.defaultStore = options.defaultStore;
    if(hooks){
        Vue.registerHooks(hooks)
    }
}

export default {
    install: (app:App, options:VueClassConfig,...args:any[]) => {
        (<any>app.config).unwrapInjectedRef = true
        setDefaultOptions(options);
    }
}
