
import vuePlugin from  './core/vuePlugin'

export {setDefaultOptions,type VueClassConfig} from  './core/vuePlugin'

export {Vue} from './core/vue'
export type {PropOptions,VueConstructor,VueBase,DefaultFactory,PublicProps,ClassComponentHooks,VueWithProps,ExtractProps,
    ExtractDefaultProps,VueMixin,DefaultKeys,WithDefault,VueStatic,OptionsContext,DataFactory,OptionsFactoryHandle,OptionsFactory,
    DataPropOptions,DataPropWrapMode} from './core/vue'

export {Options} from './decorators/options'
export {Watch} from './decorators/watch'
export {WrapMode} from './decorators/WrapMode'

export {Model} from './decorators/model'
export {Prop} from './decorators/prop'
export {Action} from './decorators/action'
export {Getter} from './decorators/getter'
export {State} from './decorators/state'
export {Mutation} from './decorators/mutation'
export {Inject,type InjectOptions} from './decorators/inject'
export {Provide,type ProvideOptions} from './decorators/provide'

export {createDecorator,getOptionsByClass,mergeSetup,mixins} from './core/helpers'
export type {MixedVueBase,UnionToIntersection,ExtractInstance,VueDecorator} from './core/helpers'

export default vuePlugin


